import React, {Component} from 'react';
import postService from '../services/post-service.js';
import PostCreateForm from './post-create-form.js';
import PostList from './post-list.js';

class Posts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: []
        };

        this.postsChangeHandler = this.postsChangeHandler.bind(this);
    }

    componentDidMount() {
        this.syncList();
    }

    postsChangeHandler () {
        this.syncList();
    }

    syncList() {
        postService.readAll()
            .then(posts => {
                this.setState({ posts });

                return posts;
            })
            .catch(err => console.log(err));
    }

    render() {
        return (
            <div className="row posts">
                <div className="col-sm-6">
                    <PostCreateForm onItemCreated={this.postsChangeHandler}/>
                </div>
                <div className="col-sm-6">
                    <PostList posts={this.state.posts} onItemRemoved={this.postsChangeHandler}/>
                </div>
            </div>
        )
    }
}

export default Posts;
