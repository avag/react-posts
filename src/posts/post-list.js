
import React, {Component} from 'react';
import postService from '../services/post-service.js';
import PostListItem from './post-list-item.js';

class PostList extends Component {
    constructor(props) {
        super(props);

        this.itemRemoveHandler = this.itemRemoveHandler.bind(this);
    }

    itemRemoveHandler (id) {
        postService.remove(id)
            .then(_ => {
                this.props.onItemRemoved && this.props.onItemRemoved(id);
            })
            .catch(err => console.error(err))
    }

    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Content</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {this.props.posts.map(post => <PostListItem key={post.id} post={post} onRemove={this.itemRemoveHandler}/>)}
                </tbody>
            </table>
        )
    }
}

export default PostList;