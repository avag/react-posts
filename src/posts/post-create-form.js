import React, {Component} from 'react';
import postService from '../services/post-service.js';

class PostCreateForm extends Component {
    constructor() {
        super();

        this.state = {
            submittion_message: '',
            error_message: '',
            title: '',
            category: '',
            content: ''
        };

        this.submitHandler = this.submitHandler.bind(this);
    }

    //Handlers
    submitHandler(evt) {
        evt.preventDefault();

        if (!this.state.title || !this.state.category || !this.state.content) {
            this.showMessage('All fields are required. Please fill them before subittion!', true);
            return;
        }

        this.setState({ submittion_message: 'Processing...' });

        postService.create(this.state.title, this.state.category, this.state.content)
            .then(_ => {
                this.setState({ title: '', category: '', content: '' });
                this.showMessage('Successfully created');

                this.props.onItemCreated && this.props.onItemCreated();
            })
            .catch(err => {
                this.showMessage('Failed to create. Reason -> ' + err.message, true);
            })

    }

    showMessage(message, isError) {
        const field = isError ? 'error_message' : 'submittion_message';

        this.setState({[field]: message});

        setTimeout(() => {
            this.setState({[field]: ''});
        }, 5000);
    }



    changeHandlerFactory(fieldName) {
        return (evt) => {
            this.setState({[fieldName]: evt.target.value})
        };
    }
    // |Handlers

    render() {
        return (<form className="form-horizontal" action="" method="POST" onSubmit={this.submitHandler}>
            {this.state.error_message && <div className="alert alert-danger">{this.state.error_message}</div>}
            <div className="form-group">
                <label htmlFor="titleTxt" className="col-sm-2 control-label">Title</label>
                <div className="col-sm-10">
                    <input type="text" name="title" className="form-control" id="titleTxt" value={this.state.title} onChange={this.changeHandlerFactory('title')} />
                </div>
            </div>
            <div className="form-group">
                <label htmlFor="categoryTxt" className="col-sm-2 control-label">Category</label>
                <div className="col-sm-10">
                    <input type="text" name="category" className="form-control" id="categoryTxt" value={this.state.category} onChange={this.changeHandlerFactory('category')}/>
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="contentTxt" className="col-sm-2 control-label">Content</label>
                <div className="col-sm-10">
                    <textarea rows="5" name="content" className="form-control" id="contentTxt" value={this.state.content} onChange={this.changeHandlerFactory('content')}>
                    </textarea>
                </div>
            </div>

            <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                    <button type="submit" className="btn btn-default">Create</button>
                </div>
            </div>
            {this.state.submittion_message && <div className="alert alert-info">{this.state.submittion_message}</div>}
        </form>)
    }

}

export default PostCreateForm;