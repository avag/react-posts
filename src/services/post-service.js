import Config from '../config.js';

function PostService() {
    this.readAll = readAll;
    this.readOne = readOne;
    this.create = create;
    this.remove = remove;

    function readAll() {
        return fetch(`${Config.POST_BASE_URI}/?key=${Config.POST_API_KEY}`).then(res => res.json());
    }

    function readOne(postId) {
        return fetch(`${Config.POST_BASE_URI}/${postId}?key=${Config.POST_API_KEY}`).then(res => res.json());
    }

    function create(title, category, content) {
        return fetch(`${Config.POST_BASE_URI}/?key=${Config.POST_API_KEY}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body:`title=${encodeURIComponent(title)}&categories=${encodeURIComponent(category)}&content=${encodeURIComponent(content)}`
        });
    }

    function remove(postId) {
        return fetch(`${Config.POST_BASE_URI}/${postId}?key=${Config.POST_API_KEY}`, {
            method: 'DELETE'
        });
    }
}

export default new PostService()