import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Posts from './posts';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">BLOG POSTS CRUD</h1>
        </header>
        <br/>
        <div className="container">
          <Posts />
        </div>
      </div>
    );
  }
}

export default App;
